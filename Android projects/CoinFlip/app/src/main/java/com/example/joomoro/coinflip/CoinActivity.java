package com.example.joomoro.coinflip;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import static com.example.joomoro.coinflip.R.id.mvoitto;
import static com.example.joomoro.coinflip.R.id.textView;
import static com.example.joomoro.coinflip.R.id.voitotYhteensa;

public class CoinActivity extends AppCompatActivity {

    Button b_flip;

    Button veikkaus;

    ImageView iv_coin;

    Random r;

    int coinSide; // 0-Kruuna 1-Klaava

    TextView omaVeikkaus;
    TextView omaVeikkausTesti;

    String arvaus;

    SeekBar seekbar;

    TextView syotettyPanos;
    TextView voitettavaSumma;
    TextView kaikkiVoitot;
    int test;
    int voitotAlussa = 200;
    int progress = 5;
    int hihi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coin);

        b_flip = (Button) findViewById(R.id.b_flip);

        veikkaus = (Button) findViewById(R.id.veikkaus);

        iv_coin = (ImageView) findViewById(R.id.iv_coin);

        r = new Random();

        kaikkiVoitot = (TextView) findViewById(R.id.voitotYhteensa);
        kaikkiVoitot.setText(""+ voitotAlussa);

        veikkaus.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                omaVeikkaus = (TextView) findViewById(R.id.veikkaus);
                arvaus = omaVeikkaus.getText().toString();

                omaVeikkausTesti = (TextView) findViewById(R.id.veikkaustesti);
                omaVeikkausTesti.setText("" + arvaus);

            }
        });

        b_flip.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v){
                omaVeikkaus = (TextView) findViewById(R.id.veikkaus);
                arvaus = omaVeikkaus.getText().toString();
                coinSide = r.nextInt(2);

                if(coinSide == 0) {
                    iv_coin.setImageResource(R.drawable.kruuna);
                    Toast.makeText(CoinActivity.this, "Kruuna!", Toast.LENGTH_SHORT).show();

                    if(arvaus == "KRUUNA" ) {

                        hihi = voitotAlussa += test;
                        kaikkiVoitot.setText(""+ hihi);
                    }
                    else{
                        hihi = voitotAlussa -= test;
                        kaikkiVoitot.setText(""+ hihi);
                    }
                } else if(coinSide == 1){
                    iv_coin.setImageResource(R.drawable.klaava);
                    Toast.makeText(CoinActivity.this, "Klaava!", Toast.LENGTH_SHORT).show();
                    if(arvaus == "KLAAVA" ) {

                        hihi = voitotAlussa += test;
                        kaikkiVoitot.setText(""+ hihi);
                    }
                    else{
                        hihi = voitotAlussa -= test;
                        kaikkiVoitot.setText(""+ hihi);
                    }
                }

                RotateAnimation rotate = new RotateAnimation(0, 360,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
                rotate.setDuration(1000);
                iv_coin.startAnimation(rotate);


            }

        });

        seekbar = (SeekBar) findViewById(R.id.seekBar);
        seekbar.setMax(100);
        seekbar.setProgress(progress);

        syotettyPanos = (TextView) findViewById(R.id.panos);
        syotettyPanos.setText(""+progress);

        voitettavaSumma = (TextView) findViewById(R.id.mvoitto);
        voitettavaSumma.setText(""+progress * 2);

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                progress = i;
                syotettyPanos.setText(""+progress);
                voitettavaSumma.setText(""+progress * 2);
                test = (progress * 2);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
}
