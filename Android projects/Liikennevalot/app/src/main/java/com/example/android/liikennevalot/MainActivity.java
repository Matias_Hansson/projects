package com.example.android.liikennevalot;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView redLight;
    private ImageView yellowLight;
    private ImageView greenLight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        redLight = (ImageView) findViewById(R.id.redLight);
        yellowLight = (ImageView) findViewById(R.id.yellowLight);
        greenLight = (ImageView) findViewById(R.id.greenLight);

    }

    public void turnOnRedLight(View view){
        resetLights();
        redLight.setImageResource(R.drawable.red_on);
    }

    public void turnOnYellowLight(View view){
        resetLights();
        yellowLight.setImageResource(R.drawable.yellow_on);
    }
    public void turnOnGreenLight(View view){
        resetLights();
        greenLight.setImageResource(R.drawable.green_on);
    }

    public void resetLights(){
        redLight.setImageResource(R.drawable.light_off);
        yellowLight.setImageResource(R.drawable.light_off);
        greenLight.setImageResource(R.drawable.light_off);
    }
}
