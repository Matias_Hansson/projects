package com.example.android.kauppalista1;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends ListActivity {

    //String[] kauppalista = {"Sissijuustoa", "Keppanaa", "Tonnikalaa", "Buranaa"};

    ArrayList<String> kauppalista = new ArrayList<String>();

    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        kauppalista.add("Maitoa");
        kauppalista.add("Leipää");
        kauppalista.add("Pullaa");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, kauppalista);

        setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Toast.makeText(MainActivity.this, "Poistettu " + kauppalista.get(position), Toast.LENGTH_LONG).show();

        kauppalista.remove(position);

        adapter.notifyDataSetChanged();
    }

    public void lisaa(View view) {

        EditText lisaa = (EditText) findViewById(R.id.lisattava);
        String lisattava = lisaa.getText().toString();

        if (lisattava.equals("")) {
            Toast.makeText(MainActivity.this, "ÄP ÄP ÄP!", Toast.LENGTH_LONG).show();
        }
        else {
            kauppalista.add(lisattava);
            adapter.notifyDataSetChanged();
            lisaa.setText("");
        }
    }
}
