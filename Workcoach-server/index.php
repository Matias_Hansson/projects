<?php
  session_start();
?>

<!DOCTYPE html>
<html>
  <head>
	<title>Workcoach</title>
	<meta name="viewport" content="width=device-width, initial-scale=1" charset="UTF-8">
	
	<!-- Css -->
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.1/jquery.mobile-1.2.1.min.css"
	<!--  href="css/jquery.mobile-1.2.1.min.css" -->
	<link rel="stylesheet" href="css/main.css">
	
	<!-- Js -->
	<script src="js/jquery-1.8.2.min.js"></script>
	<script src="js/jquery.mobile-1.2.1.min.js"></script>
	
</head>
  <body>
	
    <div data-role="page" id="main" class="tausta">
      <?php if (isset($_SESSION['id'],$_SESSION['uid'])) {
      echo '<div data-role="header" data-theme="b">';
        echo '<h1>Workcoach</h1>'; 
        echo '<center>Olet kirjautunut käyttäjänä ' . $_SESSION['uid'] . '</center>'; 
        echo '</div>'; 
      ?>
      
      <div data-role="main" id="login-signup" class="ui-content">
        
            <form data-ajax="false" action="logout.php">
              <button type="submit">
                Kirjaudu ulos
              </button>  
            </form>
          <?php } else {
  
  echo '<div data-role="header" data-theme="b">';
        echo '<h1>Workcoach</h1>';
        echo '&nbsp';
        echo '</div>'; ?>
	
        <form data-ajax="false" action="login.php" method="POST" style="margin:20px">
          <h2>Sisäänkirjautuminen</h2>
          <label for="text-basic">Käyttäjänimi:</label>
          <input type="text" name="uid" id="text-basic" value="">
          <label for="password">Salasana:</label>
          <input type="password" name="pwd" id="password" value="" autocomplete="off">
          <button name="login-btn" id="login-btn">Kirjaudu sisään</button>
          
        </form>
        <form data-ajax="false" action="signup.php" method="POST" style="margin:20px">
          <h2>Rekisteröityminen</h2>
          <label for="text-basic">Etunimi:</label>
          <input type="text" name="first" id="text-basic" value="">
          <label for="text-basic">Sukunimi:</label>
          <input type="text" name="last" id="text-basic" value="">
          <label for="text-basic">Käyttäjänimi*:</label>
          <input type="text" name="uid" id="text-basic" value="">
          <label for="text-basic">Salasana*:</label>
          <input type="password" name="pwd" id="text-basic" value="">
          <button type="submit">Rekisteröidy</button> 
        </form>
        <?php } ?>
      </div>
    </div>
  </body>
</html>