
public class Kirja {

	private String teoksenNimi;

	private int julkaisuvuosi; 

	private String tekij�;
	
	public String getTeoksenNimi() {
		return teoksenNimi;
	}
	
	public void setTeoksenNimi(String teoksenNimi) {
		this.teoksenNimi = teoksenNimi;
	}
	
	public int getJulkaisuvuosi() {
		return julkaisuvuosi;
	}
	
	public void setJulkaisuvuosi(int julkaisuvuosi) {
		this.julkaisuvuosi = julkaisuvuosi;
	}
	
	public String getTekij�() {
		return tekij�;
	}
	
	public void setTekij�(String tekij�) {
		this.tekij� = tekij�;
	}
	
	public Kirja() {
		
		this.teoksenNimi = "Ei asetettu";
		this.julkaisuvuosi = 0;
		this.tekij� = "Tuntematon";
	}
	
	@Override
	public String toString() {
		return "Kirja [teoksenNimi=" + teoksenNimi + ", julkaisuvuosi=" + julkaisuvuosi + ", tekij�=" + tekij� + "]";
	}
	
}