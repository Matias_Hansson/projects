package com.example.android.h4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // tämä rivi kirjottaa dataa logiin ja sitä käytetään debuggaukseen
        Log.i("Tag", "Ollaan ekassa ikkunassa - onCreate() metodissa");
    }

    public void klikkaus(View v) {

        // Tämä rivi kirjoittaa dataa logiin ja sitä käytetään debuggauskeen
        Log.i("Tag", "Ollaan ekassa ikkunassa - klikkaus metodissa");

        // Luodaan uusi Intent-tyyppinen muuttuja. Annetaan sille kutsujaksi tämä luokka(this) ja
        // kutsutaan äsken luomamme uuden luokan nimi "AnotherActivity.class"

        Intent intent =  new Intent(this, AnotherActivity.class);

        // Käynnistetään uuden näytön kutsuva Intent
        startActivity(intent);
        finish();
    }
}
