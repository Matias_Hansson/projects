<?php
session_start();
if (!isset($_SESSION['id'])) {
header("Location: index.php");
} ?>

<!DOCTYPE html> 
<html>
<head>
	<title>Workcoach</title>
	<meta name="viewport" content="width=device-width, initial-scale=1" charset="UTF-8">
	
	<!-- Css -->
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.1/jquery.mobile-1.2.1.min.css"
	<!--  href="css/jquery.mobile-1.2.1.min.css" -->
	<link rel="stylesheet" href="css/main.css">
	
	<!-- Js -->
	<script src="js/jquery-1.8.2.min.js"></script>
	<script src="js/jquery.mobile-1.2.1.min.js"></script>
	
</head>

<script>
$(document).on("pagecreate",function(){
// Tap fade
	$("#tap").on("tap",function(){
		$(this).fadeToggle(1000);
	});
//Taphold fade
	$("#hold").on("taphold",function(){
		$(this).fadeToggle(1000);
		$(this).fadeToggle(1000);
		});
//Swipe
	$("#swipe").on("swipe",function() {
		// tekstinvaihtotesti $(this).text("Swipe detected!");
		//alert("Swipe detected!");
		$(this).fadeToggle(1000);
	});
//Swipeleft
	$("#sleft").on("swipeleft",function() {
		//alert("You swiped left!");
		$(this).fadeToggle(1000);
	});
//Swiperight
	$("#sright").on("swiperight",function() {
		//alert("You swiped right!");
		$(this).fadeToggle(1000);
	});
//Reset
	$("#reset").click(function() {
		$("#tap,#hold,#swipe,#sleft,#sright").show();
	});
});
</script>

<body>

		<form data-ajax="false" action="submit.php" method="POST">
	<div data-role="page" id="main" class="tausta">
		<div data-role="header" data-theme="b" style="padding-right:50px">
			<h1>Workcoach</h1>
			
					<?php echo '<center>Olet kirjautunut käyttäjänä ' . $_SESSION['uid'] . '</center>';
					?>
			
	<div data-role="controlgroup" data-type="horizontal">		
  <a data-ajax="false" href="haku.php" data-icon="delete" class="ui-btn-inline">Hallintapaneeli</a>
  <a data-ajax="false" href="logout.php" data-icon="delete" class="ui-btn-inline">Kirjaudu ulos</a>
		</div>

		
		</div>
			<div style="margin:2em;">
				<a href="#k1" data-role="button" data-theme="a" data-icon="arrow-r" data-iconpos="right">Aloita vastaaminen</a>
				
			</div>
		
		
		<div style="margin:4em;">
		<center><h1>Kysymykset</h1></center>
			
			<ul data-role="listview" data-inset="true">
				<li><a href="#k1"><h2>Kysymys 1</h2><p>Työn aloitus </p></a></li>
				<li><a href="#k2"><h2>Kysymys 2</h2><p>Tauotus</p></a></li>
				<li><a href="#k3"><h2>Kysymys 3</h2><p>Onnistuminen</p></a></li>
				<li><a href="#k4"><h2>Kysymys 4</h2><p>Työmotivaatio</p></a></li>
				<li><a href="#k5"><h2>Kysymys 5</h2><p>Työilmapiiri </p></a></li>
				<li><a href="#k6"><h2>Kysymys 6</h2><p>Työn kuormitus </p></a></li>
				<li><a href="#k7"><h2>Kysymys 7</h2><p>Stressi </p></a></li>
				<li><a href="#k8"><h2>Kysymys 8</h2><p>Työpäivän päätös </p></a></li>
			</ul>
		</div>

	</div>

	
	<!----- Kysymys 1 ----->
	<div data-role="page" id="k1" class="tausta">
	
		<div data-role="header" data-theme="b">
			<a href="#main" data-icon="home">Alkuun</a>
			<h1>Kysymys 1</h1>
			<a data-ajax="false" href="logout.php" data-icon="delete" class="ui-btn-right">Kirjaudu ulos</a>
				<div data-role="navbar">
					<ul>
						<li><a href="#k1" data-icon="back" class="ui-disabled">Edellinen</a></li>
						<li><a href="#k2" data-icon="forward">Seuraava</a></li>
					</ul>
				</div>
		</div>
		
		<div style="margin:2em;">
			<h2>Työn aloitus</h2>
			
				
					<fieldset data-role="controlgroup">
				<legend>Aloitin työpäiväni virkeänä:</legend>
				<input type="radio" name="radio-choice-1" id="k1v1" value="1"  />
				<label for="k1v1">Täysin eri mieltä</label>

				<input type="radio" name="radio-choice-1" id="k1v2" value="2"  />
				<label for="k1v2">Osittain eri mieltä</label>

				<input type="radio" name="radio-choice-1" id="k1v3" value="3"  checked/>
				<label for="k1v3">En osaa sanoa</label>

				<input type="radio" name="radio-choice-1" id="k1v4" value="4"  />
				<label for="k1v4">Osittain samaa mieltä</label>

				<input type="radio" name="radio-choice-1" id="k1v5" value="5"  />
				<label for="k1v5">Täysin samaa mieltä</label>
			</fieldset>	
			
			<div data-role="fieldcontain">
				<label for="textarea">Vapaa palaute:</label>
				<textarea name="textarea1" id="textarea"></textarea>
			</div>

		</div>
		
	</div>

	<!----- Kysymys 2 ----->
	<div data-role="page" id="k2" class="tausta">
		<div data-role="header" data-theme="b">
			<a href="#main" data-icon="home">Alkuun</a>
			<h1>Kysymys 2</h1>
			<a data-ajax="false" href="logout.php" data-icon="delete" class="ui-btn-right">Kirjaudu ulos</a>
				<div data-role="navbar">
					<ul>
						<li><a href="#k1" data-icon="back">Edellinen</a></li>
						<li><a href="#k3" data-icon="forward">Seuraava</a></li>
					</ul>
				</div>
		</div>

		
		<div style="margin:2em;">
			<h2>Tauotus</h2>
			<fieldset data-role="controlgroup">
				<legend>pidin riittävästi taukoja työssäni:</legend>
				<input type="radio" name="radio-choice-2" id="k2v1" value="1" />
				<label for="k2v1">Täysin eri mieltä</label>

				<input type="radio" name="radio-choice-2" id="k2v2" value="2"  />
				<label for="k2v2">Osittain eri mieltä</label>

				<input type="radio" name="radio-choice-2" id="k2v3" value="3"  checked/>
				<label for="k2v3">En osaa sanoa</label>

				<input type="radio" name="radio-choice-2" id="k2v4" value="4"  />
				<label for="k2v4">Osittain samaa mieltä</label>

				<input type="radio" name="radio-choice-2" id="k2v5" value="5"  />
				<label for="k2v5">Täysin samaal mieltä</label>
			</fieldset>	
			
			<div data-role="fieldcontain">
				<label for="textarea">Vapaa palaute:</label>
				<textarea name="textarea2" id="textarea"></textarea>
			</div>
		</div>
		
	</div>

	<!----- Kysymys 3 ----->
	<div data-role="page" id="k3" class="tausta">
		<div data-role="header" data-theme="b">
			<a href="#main" data-icon="home">Alkuun</a>
			<a data-ajax="false" href="logout.php" data-icon="delete" class="ui-btn-right">Kirjaudu ulos</a>
			<h1>Kysymys 3</h1>
				<div data-role="navbar">
					<ul>
						<li><a href="#k2" data-icon="back">Edellinen</a></li>
						<li><a href="#k4" data-icon="forward">Seuraava</a></li>
					</ul>
				</div>
		</div>
		
		<div style="margin:2em;">
			<h2>Onnistuminen</h2>
			<fieldset data-role="controlgroup">
				<legend>Onnistuin työtehtävässäni:</legend>
				<input type="radio" name="radio-choice-3" id="k3v1" value="1" />
				<label for="k3v1">Täysin eri mieltä</label>

				<input type="radio" name="radio-choice-3" id="k3v2" value="2"  />
				<label for="k3v2">Osittain eri mieltä</label>

				<input type="radio" name="radio-choice-3" id="k3v3" value="3"  checked/>
				<label for="k3v3">En osaa sanoa</label>

				<input type="radio" name="radio-choice-3" id="k3v4" value="4"  />
				<label for="k3v4">Osittain samaa mieltä</label>

				<input type="radio" name="radio-choice-3" id="k3v5" value="5"  />
				<label for="k3v5">Täysin samaal mieltä</label>
			</fieldset>	
			
			<div data-role="fieldcontain">
				<label for="textarea">Vapaa palaute:</label>
				<textarea name="textarea3" id="textarea"></textarea>
			</div>
		</div>
		
	</div>

	<!----- Kysymys 4 ----->
	<div data-role="page" id="k4" class="tausta">
		<div data-role="header" data-theme="b">
			<a href="#main" data-icon="home">Alkuun</a>
			<a data-ajax="false" href="logout.php" data-icon="delete" class="ui-btn-right">Kirjaudu ulos</a>
			<h1>Kysymys 4</h1>
				<div data-role="navbar">
					<ul>
						<li><a href="#k3" data-icon="back">Edellinen</a></li>
						<li><a href="#k5" data-icon="forward">Seuraava</a></li>
					</ul>
				</div>
		</div>
		
		<div style="margin:2em;">
			<h2>Työmotivaatio</h2>
			<fieldset data-role="controlgroup">
				<legend>Työ on mielekästä, merkityksellistä ja nautin siitä:</legend>
				<input type="radio" name="radio-choice-4" id="k4v1" value="1"  />
				<label for="k4v1">Täysin eri mieltä</label>

				<input type="radio" name="radio-choice-4" id="k4v2" value="2"  />
				<label for="k4v2">Osittain eri mieltä</label>

				<input type="radio" name="radio-choice-4" id="k4v3" value="3"  checked/>
				<label for="k4v3">En osaa sanoa</label>

				<input type="radio" name="radio-choice-4" id="k4v4" value="4"  />
				<label for="k4v4">Osittain samaa mieltä</label>

				<input type="radio" name="radio-choice-4" id="k4v5" value="5"  />
				<label for="k4v5">Täysin samaal mieltä</label>
			</fieldset>	
			
			<div data-role="fieldcontain">
				<label for="textarea">Vapaa palaute:</label>
				<textarea name="textarea4" id="textarea"></textarea>
			</div>
		</div>
		
	</div>

	<!----- Kysymys 5 ----->
	<div data-role="page" id="k5" class="tausta">
		<div data-role="header" data-theme="b">
			<a href="#main" data-icon="home">Alkuun</a>
			<a data-ajax="false" href="logout.php" data-icon="delete" class="ui-btn-right">Kirjaudu ulos</a>
			<h1>Kysymys 5</h1>
				<div data-role="navbar">
					<ul>
						<li><a href="#k4" data-icon="back">Edellinen</a></li>
						<li><a href="#k6" data-icon="forward">Seuraava</a></li>
					</ul>
				</div>
		</div>
		
		<div style="margin:2em;">
			<h2>Työilmapiiri</h2>
			<fieldset data-role="controlgroup">
				<legend>Töissä on mukava olla ja saan rakentavaa palautetta työstäni. Viihdyn työpaikalla hyvin:</legend>
				<input type="radio" name="radio-choice-5" id="k5v1" value="1"  />
				<label for="k5v1">Täysin eri mieltä</label>

				<input type="radio" name="radio-choice-5" id="k5v2" value="2"  />
				<label for="k5v2">Osittain eri mieltä</label>

				<input type="radio" name="radio-choice-5" id="k5v3" value="3"  checked/>
				<label for="k5v3">En osaa sanoa</label>

				<input type="radio" name="radio-choice-5" id="k5v4" value="4"  />
				<label for="k5v4">Osittain samaa mieltä</label>

				<input type="radio" name="radio-choice-5" id="k5v5" value="5"  />
				<label for="k5v5">Täysin samaal mieltä</label>
			</fieldset>	
			
			<div data-role="fieldcontain">
				<label for="textarea">Vapaa palaute:</label>
				<textarea name="textarea5" id="textarea"></textarea>
			</div>
		</div>
		
	</div>

	<!----- Kysymys 6 ----->
	<div data-role="page" id="k6" class="tausta">
		<div data-role="header" data-theme="b">
			<a href="#main" data-icon="home">Alkuun</a>
			<a data-ajax="false" href="logout.php" data-icon="delete" class="ui-btn-right">Kirjaudu ulos</a>
			<h1>Kysymys 6</h1>
				<div data-role="navbar">
					<ul>
						<li><a href="#k5" data-icon="back">Edellinen</a></li>
						<li><a href="#k7" data-icon="forward">Seuraava</a></li>
					</ul>
				</div>
		</div>
		
		<div style="margin:2em;">
			<h2>Työn kuormitus</h2>
			<fieldset data-role="controlgroup">
				<legend>Sain suunnitellut päivän työt tehtyä:</legend>
				<input type="radio" name="radio-choice-6" id="k6v1" value="1" />
				<label for="k6v1">Täysin eri mieltä</label>

				<input type="radio" name="radio-choice-6" id="k6v2" value="2"  />
				<label for="k6v2">Osittain eri mieltä</label>

				<input type="radio" name="radio-choice-6" id="k6v3" value="3"  checked/>
				<label for="k6v3">En osaa sanoa</label>

				<input type="radio" name="radio-choice-6" id="k6v4" value="4"  />
				<label for="k6v4">Osittain samaa mieltä</label>

				<input type="radio" name="radio-choice-6" id="k6v5" value="5"  />
				<label for="k6v5">Täysin samaal mieltä</label>
			</fieldset>	
			
			<div data-role="fieldcontain">
				<label for="textarea">Vapaa palaute:</label>
				<textarea name="textarea6" id="textarea"></textarea>
			</div>
		</div>
		
	</div>

	<!----- Kysymys 7 ----->
	<div data-role="page" id="k7" class="tausta">
		<div data-role="header" data-theme="b">
			<a href="#main" data-icon="home">Alkuun</a>
			<a data-ajax="false" href="logout.php" data-icon="delete" class="ui-btn-right">Kirjaudu ulos</a>
			<h1>Kysymys 7</h1>
				<div data-role="navbar">
					<ul>
						<li><a href="#k6" data-icon="back">Edellinen</a></li>
						<li><a href="#k8" data-icon="forward">Seuraava</a></li>
					</ul>
				</div>
		</div>
		
		<div style="margin:2em;">
			<h2>Stressi</h2>
			<fieldset data-role="controlgroup">
				<legend>En kokenut negatiivista stressiä työssäni:</legend>
				<input type="radio" name="radio-choice-7" id="k7v1" value="1"  />
				<label for="k7v1">Täysin eri mieltä</label>

				<input type="radio" name="radio-choice-7" id="k7v2" value="2"  />
				<label for="k7v2">Osittain eri mieltä</label>

				<input type="radio" name="radio-choice-7" id="k7v3" value="3"  checked/>
				<label for="k7v3">En osaa sanoa</label>

				<input type="radio" name="radio-choice-7" id="k7v4" value="4"  />
				<label for="k7v4">Osittain samaa mieltä</label>

				<input type="radio" name="radio-choice-7" id="k7v5" value="5"  />
				<label for="k7v5">Täysin samaal mieltä</label>
			</fieldset>	
			
			<div data-role="fieldcontain">
				<label for="textarea">Vapaa palaute:</label>
				<textarea name="textarea7" id="textarea"></textarea>
			</div>
		</div>
		
	</div>
	<!----- Kysymys 8 ----->
	<div data-role="page" id="k8" class="tausta">
		<div data-role="header" data-theme="b">
			<a href="#main" data-icon="home">Alkuun</a>
			<a data-ajax="false" href="logout.php" data-icon="delete" class="ui-btn-right">Kirjaudu ulos</a>
			<h1>Kysymys 8</h1>
				<div data-role="navbar">
					<ul>
						<li><a href="#k7" data-icon="back">Edellinen</a></li>
						<li><a href="#k8" data-icon="forward" class="ui-disabled">Seuraava</a></li>
					</ul>
				</div>
		</div>
		
		<div style="margin:2em;">
			<h2>Työpäivän päätös</h2>
			<fieldset data-role="controlgroup">
				<legend>Lähdin hyvällä mielellä töistä:</legend>
				<input type="radio" name="radio-choice-8" id="k8v1" value="1"  />
				<label for="k8v1">Täysin eri mieltä</label>

				<input type="radio" name="radio-choice-8" id="k8v2" value="2"  />
				<label for="k8v2">Osittain eri mieltä</label>

				<input type="radio" name="radio-choice-8" id="k8v3" value="3"  checked/>
				<label for="k8v3">En osaa sanoa</label>

				<input type="radio" name="radio-choice-8" id="k8v4" value="4"  />
				<label for="k8v4">Osittain samaa mieltä</label>

				<input type="radio" name="radio-choice-8" id="k8v5" value="5"  />
				<label for="k8v5">Täysin samaal mieltä</label>
			</fieldset>	
			
			<div data-role="fieldcontain">
				<label for="textarea">Vapaa palaute:</label>
				<textarea name="textarea8" id="textarea"></textarea>
			</div>
		</div>
	
		<div style="margin:2em;">

			<input type="submit" name="submit" value="Lähetä vastaukset">
			</form>
		</div>
		
	</div>

</body>
</html>
?>