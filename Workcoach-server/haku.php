<?php
session_start();
if ($_SESSION['uid'] != 'admin') {
header("Location: index.php");
}
	
	function connectDB() {
		$host = "localhost";
		$user = "root";
		$pass = "";
		$databaseName = "login";
		$con = mysql_connect($host,$user,$pass);
		$dbs = mysql_select_db($databaseName, $con);
	}
	function queryToArray($result) {
		$array = array();
		while ($row = mysql_fetch_row($result)) { // Fetch results
			$array[] = $row;
		}
		return $array;
	}
	function tellUsername($user_id) {
		
		connectDB();
		$result = mysql_query("SELECT uid FROM user WHERE id ='$user_id'");
		$array = mysql_fetch_row($result);
		$uid = $array[0];
		return "" . $uid;
	}
	function processJSON($json_file) {
		
		$question_list = array(
			"1" => "Aloitin työpäiväni virkeänä: ",
			"2" => "Pidin riittävästi taukoja työssäni: ",
			"3" => "Onnistuin työtehtävässäni: ",
			"4" => "Työ on mielekästä, merkityksellistä ja nautin siitä: ",
			"5" => "Töissä on mukava olla ja saan rakentavaa palautetta työstäni. Viihdyn työpaikalla hyvin: ",
			"6" => "Sain suunnitellut päivän työt tehtyä: ",
			"7" => "En kokenut negatiivista stressiä työssäni: ",
			"8" => "Lähdin hyvällä mielellä töistä: ",
		);
		
		$answer_list = array(
			"1" => "Täysin eri mieltä",
			"2" => "Osittain eri mieltä",
			"3" => "En osaa sanoa",
			"4" => "Osittain samaa mieltä",
			"5" => "Täysin samaa mieltä",
		);
		
		$answer_count = count(json_decode($json_file, true)); // How many submits
		$obj = json_decode($json_file, true); // All answers per day
		
		for($i=0; $i<$answer_count; $i++) { // Per answerer
			$user_id = $obj[$i][1];
			echo '<ul data-role="listview" data-inset="true" style="margin:2em;">';
			
			for($y=2; $y<10; $y++) { // Per answer
				$answer = $obj[$i][$y];
				$feedback = $obj[$i][$y+8];
				
				if($y == 2) {
					echo "<li>";
					echo "<b>" . tellUsername($user_id) . "</b>";
					echo "</li>";
				}
				
				if ($answer != null) { // Question & answer
					echo '<li>';
					echo '<b>' . $question_list[$y-1] . '</b>' . $answer_list[$answer];
					echo '</li>';
				}
				
				if($feedback != null) { // Feedback for answer
					echo '<li>';
					echo 'Palaute: ' . $feedback;
					echo '</li>';
				}
			}
			echo '</ul>';
		}
	}
	function optionUserList() {
		connectDB();
		$result = mysql_query("SELECT id FROM user");
		$array = queryToArray($result);
		echo '<option value="all">Kaikki</option>';
		foreach ($array as $users ) {
			foreach ($users as $user_id) {
				echo '<option value=' . $user_id . '>' . tellUsername($user_id) . '</option>';
			}
		}
	}
	//--------------------------------------------------------------------------
	// 1) Connect to mysql database
	//--------------------------------------------------------------------------
	connectDB();
	//--------------------------------------------------------------------------
	// 2) Query database for data
	//--------------------------------------------------------------------------
if (isset($_GET['date']) && isset($_GET['name'])) {
	$tableName = "vastaukset";
	$date = $_GET['date'];
	$name = $_GET['name'];
	}

	if ($name == "all") {
		$result = mysql_query("SELECT * FROM " . $tableName . " WHERE pvm = " . "'" . $date . "'");
	} else { 
		$result = mysql_query("SELECT * FROM " . $tableName . " WHERE pvm = " . "'" . $date . "'" . " AND user_id = " . "'" . $name . "'");
	}

	$array = queryToArray($result);

	//--------------------------------------------------------------------------
	// 3) Pack json 
	//--------------------------------------------------------------------------
	$json_file = json_encode($array);
?>
<!DOCTYPE html> 
<html>
	<head>
		<title>Workcoach - Hallintapaneeli</title>
		<meta charset=utf-8 />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Css -->
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.1/jquery.mobile-1.2.1.min.css">
		<link rel="stylesheet" href="http://code.jquery.com/mobile/git/jquery.mobile-git.css">
		<link rel="stylesheet" href="css/jquery.mobile.datepicker.css" />
		<link rel="stylesheet" href="css/jquery.mobile.datepicker.theme.css" />
		<link rel="stylesheet" href="css/main.css">
		<!-- Js -->
		<script src="http://code.jquery.com/mobile/git/jquery.mobile-git.js"></script>
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script> 
		<script src="js/datepicker.js"></script>
		<script src="http://code.jquery.com/mobile/git/jquery.mobile-git.js"></script> 
		<script src="js/jquery.mobile.datepicker.js"></script>

		
	</head>
	
	<body>
		<div data-role="page" id="main" class="tausta">
		<div data-role="header" data-theme="b" style="padding-right:50px">
				<h1>Workcoach</h1>
			
			<div data-role="controlgroup" data-type="horizontal">
				<a data-ajax="false" href="workcoach.php" data-icon="carat-l" class="ui-btn-c">Takaisin</a>
			</div>	
						</div>	
			
			<div style="margin:4em;">
				<center><h1>Hallintapaneeli</h1></center>
			</div>
			
			<form>
				<div class="ui-field-contain">
					<label for="select-native-1">Valitse henkilö:</label>
					<select name="select-native-1" id="select-native-1">
						<?php optionUserList(); ?>
					</select>
				</div>
			</form>
			
			<div id="kalenteri">
				<center>
					<input type="text" id="kalenteriPaiva" class="date-input-inline" data-inline="true" data-role="date" data-inline="true">
					<input type="button" id="haeTiedotNappi" value="Hae tiedot" data-theme="a" data-inline="true"/>		
				</center>		
			</div>
			<?php processJSON($json_file); // Answers ?> 
		</div>
	</body>
	
	<script>
		//haeTiedotNappi hakee etsityn henkilön tiedot
		$( document ).ready(function() {
			$("#kalenteriPaiva").datepicker({
				dateFormat: "dd/mm/yy"
			});
		});
		$("#haeTiedotNappi").click(function(){
			var valittuPaiva = displayDate();
			var valittuId = displayId();
			window.location.href = "haku.php?date=" + valittuPaiva + "&name=" + valittuId;
		});
		
		function displayDate(){
			return $( "#kalenteriPaiva" ).val();
		};
		
		function displayId() {
			return $( "#select-native-1" ).val();
		}
	</script>
</html>