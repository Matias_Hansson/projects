package com.example.android.korkolaskuri;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void haeArvot (View view) {

    // Haetaan ensin viite tekstikenttään ja luetaan sisältö teksti -muuttujaan

        // Tallennetaan alkupääoman tiedot alkupaaomateksti muuttujaan
            EditText kentta = (EditText) findViewById(R.id.alkupaaoma);
            String alkuPteksti = kentta.getText().toString();

        // Tallennetaan korko-kentän tiedot korkoteksti muuttujaan
            EditText kentta1 = (EditText) findViewById(R.id.korko);
            String korkoteksti = kentta1.getText().toString();

            // Tallennetaan korko-kentän tiedot korkoteksti muuttujaan
            EditText kentta2 = (EditText) findViewById(R.id.lainaaika);
            String lainateksti = kentta2.getText().toString();

    // Näytetään haettu arvo Toastin avulla
        //Toast.makeText(this, "Kirjoitit: " + lainateksti, Toast.LENGTH_LONG).show();

    //Tekstikentästä luettu data palautetaan aina String-muodossa, se pitää muuttaa luvuksi laskemista varten
        double paaomaLukuna = Double.parseDouble(alkuPteksti);
        double korkoLukuna = Double.parseDouble(korkoteksti);
        double lainaaikaLukuna = Double.parseDouble(lainateksti);

    // Lopuksi lasketaan loppusumma korolle kaavan mukaan

        double tulos = paaomaLukuna * (korkoLukuna / 100) * lainaaikaLukuna;

    // Tulos pitää esittää vielä tekstinä
        String tulosTekstinä = Double.toString(tulos);

    // Asettaa saatu tulos lopputulos-tekstikenttän arvoksi

        TextView lopputulos = (TextView) findViewById(R.id.lopputulos);
        lopputulos.setText ("Korko: " + tulosTekstinä);
    }
}
